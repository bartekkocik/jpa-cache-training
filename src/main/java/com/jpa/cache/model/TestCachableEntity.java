package com.jpa.cache.model;

import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.Id;

// 2 level cache example entity
@Cacheable // will be stored in second level cache
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Entity
public class TestCachableEntity {

    @Id
    private Long id;

    private String value;

    public TestCachableEntity() {
    }

    public TestCachableEntity(final Long id, final String value) {
        this.id = id;
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public void setValue(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
