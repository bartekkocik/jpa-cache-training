package com.jpa.cache.model;

import javax.persistence.Entity;
import javax.persistence.Id;

// 1 level cache example entity
@Entity
public class TestEntity {

    @Id
    private Long id;

    private String value;

    public TestEntity() {
    }

    public TestEntity(final Long id, final String value) {
        this.id = id;
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public void setValue(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
