package com.jpa.cache;

import com.jpa.cache.config.DatabaseConfiguration;
import com.jpa.cache.model.TestCachableEntity;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.assertj.core.api.Assertions.assertThat;

//2 level cache test
@ContextConfiguration(classes = DatabaseConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class SessionFactoryTest extends AbstractTest {

    @Test
    public void shouldBeCached() {
        // given
        TestCachableEntity testCachableEntity = new TestCachableEntity(1L, VALUE_1);
        doInTransaction(() -> {
            em.persist(testCachableEntity);
        });

        // when then
        doInTransaction(() -> {
            checkEntityInCache(testCachableEntity, true);
            TestCachableEntity fetchedTestEntity = em.find(TestCachableEntity.class, testCachableEntity.getId());
            checkEntityInCache(fetchedTestEntity, true);
            assertThat(testCachableEntity).isNotEqualTo(fetchedTestEntity);

            // clear local persistence context
            em.clear();

            checkEntityInCache(fetchedTestEntity, true);
        });
    }

    @Test
    public void shouldNotBeInCache() {
        // given
        TestCachableEntity testCachableEntity = new TestCachableEntity(2L, VALUE_1);

        doInTransaction(() -> {
            em.persist(testCachableEntity);
        });

        // when then
        doInTransaction(() -> {
            TestCachableEntity fetchedEntity = em.find(TestCachableEntity.class, testCachableEntity.getId());

            em.clear();
            em.getEntityManagerFactory().getCache().evict(TestCachableEntity.class, testCachableEntity.getId());

            checkEntityInCache(fetchedEntity, false);
        });
    }

    @After
    public void cleanUp() {
        em.getEntityManagerFactory().getCache().evictAll();
    }
}