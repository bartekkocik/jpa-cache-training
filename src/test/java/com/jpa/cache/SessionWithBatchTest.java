package com.jpa.cache;

import com.google.common.base.Stopwatch;
import com.jpa.cache.config.DatabaseConfiguration;
import com.jpa.cache.model.TestEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import static org.assertj.core.api.Assertions.assertThat;

@ContextConfiguration(classes = DatabaseConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class SessionWithBatchTest extends AbstractTest {

    private AtomicLong counter = new AtomicLong(100L);

    @Test
    public void shouldBatchBeQuickerWithContextClear() throws Exception {

        // warmup
        doInTransaction(() -> {
            for (int i = 0; i < 40_000; i++) {
                em.persist(new TestEntity(counter.getAndIncrement(), VALUE_1));
            }
        });

        System.gc();

        Stopwatch stopwatch = Stopwatch.createStarted();

        // with em.clear()
        doInTransaction(() -> {
            for (int i = 0; i < 30_000; i++) {
                em.persist(new TestEntity(counter.getAndIncrement(), VALUE_1));
                if (i % 100 == 0) {
                    em.flush();
                    em.clear();
                }
            }
        });
        long withClear = stopwatch.elapsed(TimeUnit.MILLISECONDS);

        stopwatch.reset();
        stopwatch.start();

        // without em.clear()
        doInTransaction(() -> {
            for (int i = 0; i < 30_000; i++) {
                em.persist(new TestEntity(counter.getAndIncrement(), VALUE_1));
                if (i % 100 == 0) {
                    em.flush();
                }
            }
        });
        long withoutClear = stopwatch.elapsed(TimeUnit.MILLISECONDS);

        System.out.format("with clear() %d, without clear() %d", withClear, withoutClear);
        assertThat(withClear).isLessThan(withoutClear);
    }
}
