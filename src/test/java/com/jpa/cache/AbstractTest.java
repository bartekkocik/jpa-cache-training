package com.jpa.cache;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;

abstract class AbstractTest {

    protected static final String VALUE_1 = "value1";
    protected static final String VALUE_2 = "value2";

    @PersistenceContext
    protected EntityManager em;

    @Autowired
    private PlatformTransactionManager platformTransactionManager;

    private TransactionTemplate transactionTemplate;

    @Before
    public void setUp() throws Exception {
        transactionTemplate = new TransactionTemplate(platformTransactionManager);
        transactionTemplate.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
    }

    public void doInTransaction(final Runnable runnable) {
        transactionTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(final TransactionStatus transactionStatus) {
                runnable.run();
            }
        });
    }

    protected void checkEntityInCache(Object entity, boolean entityShouldExist) {
        Cache cache = CacheManager.ALL_CACHE_MANAGERS.get(0).getCache(entity.getClass().getName());
        int numberOfElementsInCache = Objects.nonNull(cache) ? cache.getSize() : 0;
        if (entityShouldExist) {
            assertThat(numberOfElementsInCache)
                    .isGreaterThan(0)
                    .withFailMessage("Object wasn't found in cache");
        } else {
            assertThat(numberOfElementsInCache)
                    .isZero()
                    .withFailMessage("Object exist in cache");
        }
        System.out.println("Number of elements in cache " + numberOfElementsInCache);
    }
}
