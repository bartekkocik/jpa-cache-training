package com.jpa.cache;

import com.jpa.cache.config.DatabaseConfiguration;
import com.jpa.cache.model.TestCachableEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.Query;

@ContextConfiguration(classes = DatabaseConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class VisualizationCacheHipWhenSelectById extends AbstractTest {

    @Test
    public void shouldNotBeInCache() {
        // given
        TestCachableEntity testCachableEntity = new TestCachableEntity(2L, VALUE_1);

        doInTransaction(() -> {
            em.persist(testCachableEntity);
        });
        em.getEntityManagerFactory().getCache().evictAll();
        em.clear();

        System.out.println("--- test start");

        // when then
        doInTransaction(() -> {
            System.out.println("id " + testCachableEntity.getId());
            System.out.println("first select by id");
            TestCachableEntity fetchedEntity = em.find(TestCachableEntity.class, testCachableEntity.getId());


            Query nativeQuery = em.createNativeQuery("SELECT * FROM TestCachableEntity t WHERE t.value=:param", TestCachableEntity.class);
            nativeQuery.setParameter("param", VALUE_1);
            Object singleResult = nativeQuery.getSingleResult();
            System.out.println("first select by value ");

            System.out.println("second select by id");
            TestCachableEntity testCachableEntity1 = em.find(TestCachableEntity.class, testCachableEntity.getId());

            System.out.println("second select by value ");
            Query nativeQuery2 = em.createNativeQuery("SELECT * FROM TestCachableEntity w WHERE w.value=:param", TestCachableEntity.class);
            nativeQuery2.setParameter("param", VALUE_1);
            Object singleResult2 = nativeQuery2.getSingleResult();

            System.out.println("third select by id");
            TestCachableEntity testCachableEntity2 = em.find(TestCachableEntity.class, testCachableEntity.getId());
        });
    }
}
