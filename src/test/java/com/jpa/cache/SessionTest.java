package com.jpa.cache;

import com.jpa.cache.config.DatabaseConfiguration;
import com.jpa.cache.model.TestEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.FlushModeType;

import static org.assertj.core.api.Assertions.assertThat;

//1 level cache test
// persistence context test
@ContextConfiguration(classes = DatabaseConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class SessionTest extends AbstractTest {

    // TODO remove assertion and start asking questions :)
    // don't forgot about method names
    @Test
    public void shouldSaveEntity() {
        // given
        TestEntity testEntity = new TestEntity(1L, VALUE_1);

        // when
        doInTransaction(() -> em.persist(testEntity));

        // then
        doInTransaction(() -> assertThat(em.find(TestEntity.class, 1L)).isNotNull());
    }

    @Test
    public void shouldNotSaveEntity() {
        // given
        TestEntity testEntity = new TestEntity(2L, VALUE_1);

        // when
        doInTransaction(() -> {
            em.persist(testEntity);
            em.clear();
        });

        // then
        doInTransaction(() -> assertThat(em.find(TestEntity.class, 2L)).isNull());
    }

    @Test
    public void shouldSaveEntityThanksToManualFlush() {
        // given
        TestEntity testEntity = new TestEntity(3L, VALUE_1);

        // when
        doInTransaction(() -> {
            em.persist(testEntity);
            em.flush();
            em.clear();
        });

        // then
        doInTransaction(() -> assertThat(em.find(TestEntity.class, 3L)).isNotNull());
    }

    @Test
    public void shouldSaveEntityThanksToAutoFlush() {
        // given
        TestEntity testEntity = new TestEntity(4L, VALUE_1);

        // when
        doInTransaction(() -> {
            em.persist(testEntity);
            // flush by select,  AUTO flush trigger the entity state transition synchronization
            em.createQuery("select p from TestEntity p").getResultList();
            em.clear();
        });

        // then
        doInTransaction(() -> assertThat(em.find(TestEntity.class, 4L)).isNotNull());
    }

    @Test
    public void shouldSaveEntityThanksToAutoFlushOnNativeQuery() {
        // given
        TestEntity testEntity = new TestEntity(5L, "testEntity");

        // when
        doInTransaction(() -> {
            em.persist(testEntity);
            em.createNativeQuery("select * from TestEntity p").getResultList();
            em.clear();
        });

        // then
        doInTransaction(() -> assertThat(em.find(TestEntity.class, 5L)).isNotNull());
    }

    @Test
    public void shouldNotSaveEntityWithCommitFlush() {
        // given
        TestEntity testEntity = new TestEntity(6L, VALUE_1);

        // when
        doInTransaction(() -> {
            em.persist(testEntity);
            // default flush mode is FlushModeType.AUTO
            em.setFlushMode(FlushModeType.COMMIT);
            em.createQuery("select p from TestEntity p").getResultList();
            em.clear();
        });

        // then
        doInTransaction(() -> assertThat(em.find(TestEntity.class, 6L)).isNull());
    }

    @Test
    public void shouldChangeNameThanksToMerge() {
        // given
        TestEntity testEntity = new TestEntity(7L, VALUE_1);

        // when
        doInTransaction(() -> {
            em.persist(testEntity);
            em.flush();
            em.clear();
            testEntity.setValue(VALUE_2);
            em.merge(testEntity);
        });

        // then
        doInTransaction(() -> assertThat(em.find(TestEntity.class, 7L).getValue()).isEqualTo(VALUE_2));
    }

    @Test
    public void shouldNotChangeNameDueToClear() {
        // given
        TestEntity testEntity = new TestEntity(8L, VALUE_1);

        // then
        doInTransaction(() -> {
            em.persist(testEntity);
            em.flush();
            em.clear();
            testEntity.setValue(VALUE_2);
            em.flush();
        });

        // then
        doInTransaction(() -> assertThat(em.find(TestEntity.class, 8L).getValue()).isEqualTo(VALUE_1));
    }

    @Test
    public void shouldNotBeEqualsInTwoTransactions() {
        // given
        TestEntity testEntity = new TestEntity(9L, VALUE_1);

        // when
        doInTransaction(() -> {
            em.persist(testEntity);
        });

        // then
        doInTransaction(() -> {
            assertThat(testEntity).isNotEqualTo(em.find(TestEntity.class, testEntity.getId()));
        });
    }

    @Test
    public void shouldBeEqualsInOneTransaction() {
        // given
        TestEntity testEntity = new TestEntity(10L, VALUE_1);

        // when then
        doInTransaction(() -> {
            em.persist(testEntity);
            assertThat(testEntity).isEqualTo(em.find(TestEntity.class, testEntity.getId()));
        });
    }

    @Test
    public void shouldShouldNotMergeWithoutCommit() {
        // given
        TestEntity testEntity = new TestEntity(11L, VALUE_1);
        doInTransaction(() -> {
            em.persist(testEntity);
        });

        // when then
        doInTransaction(() -> {
            testEntity.setValue(VALUE_2);
            em.merge(testEntity);
            em.clear();
            TestEntity testEntity1 = em.find(TestEntity.class, 11L);
            assertThat(testEntity1.getValue()).isEqualTo(VALUE_1);
        });
    }

    @After
    public void getStatistics() {
        doInTransaction(() -> {
            Session session = em.unwrap(Session.class);
            SessionFactory sessionFactory = session.getSessionFactory();
            System.out.println(sessionFactory.getStatistics().getEntityStatistics(TestEntity.class.getName()));
        });
    }
}
