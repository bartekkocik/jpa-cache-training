package com.jpa.cache;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.google.common.base.Stopwatch;
import com.jpa.cache.config.DatabaseConfiguration;
import com.jpa.cache.model.TestEntity;

@ContextConfiguration(classes = DatabaseConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class BatchOperationBasenOnPersistantContextTest extends AbstractTest {

    private AtomicLong counter = new AtomicLong(100L);

    @Test
    public void shouldCompareTimeExecutionOnFullAndEmptyContext() {

        // warmup; pre upload
        doInTransaction(() -> {
            for (int i = 0; i < 40_000; i++) {
                em.persist(new TestEntity(counter.getAndIncrement(), "test"));
            }
        });

        // System.gc();

        Stopwatch stopwatch = Stopwatch.createStarted();

        // with em.clear()
        doInTransaction(() -> {
            for (int i = 0; i < 30_000; i++) {
                em.persist(new TestEntity(counter.getAndIncrement(), "test"));
                if (i % 100 == 0) {
                    em.flush();
                    em.clear();
                }
            }
        });
        long withClear = stopwatch.elapsed(TimeUnit.MILLISECONDS);

        stopwatch.reset();
        stopwatch.start();

        // without em.clear()
        doInTransaction(() -> {
            for (int i = 0; i < 30_000; i++) {
                em.persist(new TestEntity(counter.getAndIncrement(), "test"));
                if (i % 100 == 0) {
                    em.flush();
                }
            }
        });
        long withoutClear = stopwatch.elapsed(TimeUnit.MILLISECONDS);

        System.out.format("Processing with clear() took %d while without clear() took %d", withClear, withoutClear);

        assertThat(withClear).isLessThan(withoutClear);
    }

}
